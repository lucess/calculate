import sys
from PyQt5 import QtGui
from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QMainWindow


class MyForm(QMainWindow):
    firstvalue = 0
    secondvalue = 0
    action = ""

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        uic.loadUi("calculator_ui.ui", self)

        self.button_1.clicked.connect(self.buttonClicked)
        self.button_2.clicked.connect(self.buttonClicked)
        self.button_3.clicked.connect(self.buttonClicked)
        self.button_4.clicked.connect(self.buttonClicked)
        self.button_5.clicked.connect(self.buttonClicked)
        self.button_6.clicked.connect(self.buttonClicked)
        self.button_7.clicked.connect(self.buttonClicked)
        self.button_8.clicked.connect(self.buttonClicked)
        self.button_9.clicked.connect(self.buttonClicked)
        self.button_0.clicked.connect(self.buttonClicked)

        self.button_plus.clicked.connect(self.action_button)  # 加
        self.button_minus.clicked.connect(self.action_button)  # 减
        self.button_multi.clicked.connect(self.action_button)  # 乘
        self.button_division.clicked.connect(self.action_button)  # 除
        self.button_equal.clicked.connect(self.result)  # 等于
        self.button_C.clicked.connect(self.clean)  # 清空
        self.button_comma.clicked.connect(self.buttonClicked)  # 点
        self.button_bksp.clicked.connect(self.backspace)  # 上一步

    def buttonClicked(self):
        if self.button_equal.isEnabled() == False:
            self.lcdNumber.display(0)
            self.button_equal.setEnabled(True)
        sender = self.sender()
        x = str(self.lcdNumber.value())
        if self.button_comma.isEnabled() == True:
            x = x[:-2]
            if sender.text() == ".":
                x += sender.text()
                self.button_comma.setEnabled(False)
            else:
                if x == "0":
                    x = sender.text()
                else:
                    x += sender.text()
        else:
            if self.button_comma.isChecked() == True:
                self.button_comma.setCheckable(False)
                x = x[:-1] + sender.text()
            else:
                x += sender.text()
        if len(x) > 7:
            self.lcdNumber.setDigitCount(len(x))
        self.lcdNumber.display(x)

    def action_button(self):
        global firstvalue, action
        sender = self.sender()
        firstvalue = self.lcdNumber.value()
        self.lcdNumber.display(0)
        action = sender.text()
        self.button_comma.setEnabled(True)
        self.button_comma.setCheckable(True)
        self.lcdNumber.setDigitCount(7)

    def result(self):
        global firstvalue, secondvalue, action
        secondvalue = self.lcdNumber.value()
        if action == "+":
            x = firstvalue + secondvalue
        if action == "-":
            x = firstvalue - secondvalue
        if action == "*":
            x = firstvalue * secondvalue
        if action == "/":
            x = firstvalue / secondvalue
        x = str(x)
        if len(x) - x.index(".") > 3:
            y = len(x) - x.index(".") - 3
            x = x[:-y]
        if x.endswith(".0"):
            x = x[:-2]
        if len(x) > 7:
            self.lcdNumber.setDigitCount(len(x))
        self.lcdNumber.display(x)
        firstvalue = 0
        secondvalue = 0
        action = ""
        self.button_comma.setEnabled(True)
        self.button_comma.setCheckable(True)
        self.button_equal.setEnabled(False)

    def clean(self):
        global firstvalue, secondvalue, action
        self.lcdNumber.display(0)
        firstvalue = 0
        secondvalue = 0
        action = ""
        self.button_comma.setEnabled(True)
        self.button_comma.setCheckable(True)
        self.lcdNumber.setDigitCount(7)

    def backspace(self):
        x = str(self.lcdNumber.value())
        if self.button_comma.isEnabled() == True:
            x = x[:-3]
        else:
            x = x[:-1]
        if len(x) > 7:
            self.lcdNumber.setDigitCount(len(x))
        else:
            self.lcdNumber.setDigitCount(7)
        self.lcdNumber.display(x)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyForm()
    window.show()
    sys.exit(app.exec())
